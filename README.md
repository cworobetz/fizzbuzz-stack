# FizzBuzz-Stack

https://gitlab.com/cworobetz/fizzbuzz-stack/badges/master/pipeline.svg

FizzBuzz-Stack is the latest and greatest in FizzBuzz engineering.

Your barebones deployment of FizzBuzz gets you
 - A FizzBuzz JSON API
 - High availability
 - Load balancing
 - The speed and simplicity of the spicy new go language
 - And so much more...

## Install

There are various ways of installing FizzBuzz-Stack.

### Testing locally

If you're developing on your local environment, from the root of the repo run:

    cd fizzbuzz
    go build . -o fizzbuzz_service    
    ./fizzbuzz_service # Runs on 0.0.0.0:80

### Building with docker

If you wish to run build the docker image locally, from the root of the repo run:

    docker build --tag fizzbuzz .

After, you can run it on your host's port 80 using

    docker run -p 80:80 fizzbuzz:latest

### Using precompiled docker images

You can use the latest (and only the latest) precompiled docker image by running:

    docker run -p 80:80 registry.gitlab.com/cworobetz/fizzbuzz-stack:latest

### Using docker-compose

You can quickly set up a fizzbuzz-stack on your local environment using the provided docker-compose.yml file. docker-compose must be installed on your system, and by default it will run on port 80.

    docker-compose up -d

### Using Kubernetes

You can deploy this using the included Kubernetes configuration by running the following from the root of the repo:

    kubectl apply -f ./kubernetes/

Once it's deployed, you can get the load balancer IP using the following:

    kubectl get service fizzbuzz

Use the EXTERNAL-IP to access it.

## Usage

Once the fizzbuzz application has been deployed, you can access it via its simple api:

    curl http://localhost/15 # should return "fizzbuzz"

returns

    *   Trying 127.0.0.1...
    * TCP_NODELAY set
    * Connected to localhost (127.0.0.1) port 80 (#0)
    > GET /15 HTTP/1.1
    > Host: localhost
    > User-Agent: curl/7.58.0
    > Accept: */*
    > 
    < HTTP/1.1 200 OK
    < Content-Type: application/json
    < Hostname: fizzbuzz-7ddc5bd4db-pwt8c
    < Date: Wed, 15 Apr 2020 09:10:46 GMT
    < Content-Length: 31
    < 
    {"Status":"OK","Response":"fizzbuzz"}
    * Connection #0 to host localhost left intact

Of course, you could simply run a for loop:

    for i in range {1..15}; do curl localhost/$i; done

Returns:

    {"Status":"OK","Response":"1"}
    {"Status":"OK","Response":"2"}
    {"Status":"OK","Response":"fizz"}
    {"Status":"OK","Response":"4"}
    {"Status":"OK","Response":"buzz"}
    {"Status":"OK","Response":"fizz"}
    {"Status":"OK","Response":"7"}
    {"Status":"OK","Response":"8"}
    {"Status":"OK","Response":"fizz"}
    {"Status":"OK","Response":"buzz"}
    {"Status":"OK","Response":"11"}
    {"Status":"OK","Response":"fizz"}
    {"Status":"OK","Response":"13"}
    {"Status":"OK","Response":"14"}
    {"Status":"OK","Response":"fizzbuzz"}

If you're running it in a cluster, you can check the HTTP headers to check which backend host sent the reply.
