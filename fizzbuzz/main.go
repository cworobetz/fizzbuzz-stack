package main

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"os"
	"strconv"
)

func main() {
	fmt.Println("Starting HTTP server...")
	http.HandleFunc("/", fizzbuzzservice)

	log.Fatal(http.ListenAndServe(":80", nil))
}

func fizzbuzzservice(writer http.ResponseWriter, request *http.Request) {

	// Basic json compatible struct
	type FizzBuzzResponse struct {
		Status   string
		Response string
	}

	var response FizzBuzzResponse

	// input is anything after / in the URL, e.g. http://127.0.0.1/<input>
	input := request.URL.Path[1:]

	if input == "" {
		response.Status = "Error"
		response.Response = "Invalid input. An integer or \"all\" is required"
	} else if input == "all" {
		for i := 0; i <= 100; i++ {
			response.Response += fizzbuzz(i)
			response.Response += " "
		}
		response.Status = "OK"
	} else { // Input is not blank or "all"
		input, err := strconv.Atoi(request.URL.Path[1:])
		if err != nil {
			response = FizzBuzzResponse{"Error", "Invalid input. An integer or \"all\" is required"}
		}
		response = FizzBuzzResponse{"OK", fizzbuzz(input)}
	}

	// translate the response to json
	json, err := json.Marshal(response)
	if err != nil {
		http.Error(writer, err.Error(), http.StatusInternalServerError)
	}
	json = append(json, "\n"...)

	// append the hostname to the header. Useful for testing load balancers
	hostname, err := os.Hostname()
	if err != nil {
		log.Fatal(err)
	}
	writer.Header().Set("Hostname", hostname)

	writer.Header().Set("Content-Type", "application/json")
	writer.Write(json)
}

func fizzbuzz(number int) string {

	fizzbuzz := ""

	if number%3 == 0 {
		fizzbuzz += "fizz"
	}
	if number%5 == 0 {
		fizzbuzz += "buzz"
	}
	if fizzbuzz == "" {
		fizzbuzz = strconv.Itoa(number)
	}

	return fizzbuzz
}
