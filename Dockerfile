# golang:1.14.2
FROM golang@sha256:9100c9f7d9924cced40f9ff3872b88334ad26a20685564988aefd2d88f6e6c75 as go
WORKDIR /go
COPY /fizzbuzz /go
RUN CGO_ENABLED=0 GOOS=linux go build -o fizzbuzz_service .

# alpine:3.11.5
FROM alpine@sha256:afb25d7b78d81dd0166c9f510bfa7a1f7071fdecd5fe846ebfb2a6d691828c05 as alpine
WORKDIR /app
COPY --from=go /go/fizzbuzz_service /app
CMD ["./fizzbuzz_service"]
EXPOSE 80